<!--
vim: spelllang=fr keymap=accents
-->
# Exercice 1 de programmation pour STM32

Ce premier exercice vous guidera à installer tous les logiciels nécessaires à la
programmation de microcontrôleurs STM32. De plus, il serait l'occasion de
télécharger un programme du type ["Hello
World!"](https://fr.wikipedia.org/wiki/Hello_world) sur un microcontrôleur.

## Introduction
La famille STM32 est une série de microcontrôleurs 32-bits en circuits intégrés
réalisés par STMicroelectronics. Les cartes de développement STM (les cartes
NUCLEO) seront utilisées pour les appareils périphériques dans les robots cette
année.

## Partie A : Installation des logiciels
Une liste des logiciels à installer et de leurs utilité se trouve ci-dessous.

### [Visual Studio Code](https://code.visualstudio.com/) et [PlatformIO](https://platformio.org/platformio-ide):
Ensemble, ces deux forment notre environnement de développement. C'est ici que
nous allons écrire notre code, le compiler, et le télécharger sur les
microcontrôleurs.

Si vous avez assisté aux premiers ateliers de programmation en Python, vous
devrait déjà avoir ces deux logiciels. Sinon, contacter nous.

### [STLINK Utility][stlink]:
Les cartes de développement *STM* sont muni d'un deuxième microcontrôleur,
nommé STLINK, qui gère l'écriture des programmes via USB, la mise à jour du
micrologiciel (firmware) du microcontrôleur principale et le débogage.


*STLINK Utility* est un logiciel distribué par *STM* qui sert à communiquer
avec ce deuxième microcontrôleur STLINK. Nous allons principalement utiliser
cette utilité afin de télécharger des programmes sur les microcontrôleurs.


*STLINK Utility* peut être installé à partir [d'ici][stlink]. Si vous utilisez
un système d'exploitation autre que Windows, installez à partir du
gestionnaire des paquets de votre système d'exploitation. Par exemple, pour
macOS, suivez les directives suivantes.

1. Ouvrer l'application "Terminal":
	<kbd>Command+space</kbd> ou dans "Launcher"
1. Installer de [Homebrew][brew]:
	```
	[ -x "$(command -v brew)" ] || /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
	```
1. Installer `stlink`:
	```
	brew install stlink
	```

[stlink]: https://www.st.com/content/st_com/en/products/development-tools/software-development-tools/stm32-software-development-tools/stm32-programmers/stsw-link004.html
[brew]: https://brew.sh/

### [stm32flash][stm32flash]
*stm32flash* est très similaire à *STLINK Utility*. Cependant, il y a des
différences majeures. Premièrement, *stm32flash* est un projet à source
ouverte, pas un produit de STM. Deuxièmement, il contourne le microcontrôleur
*STLINK* et écrit directement sur le microcontrôleur principal via UART ou
I²C. La possibilité de télécharger via UART ou I²C est une avantage majeur et
sera utilisé beaucoup afin de flasher via la *Raspberry Pi*.

*stm32flash* est disponible [ici][stm32flash].

#### Installation dans *windows*
Pour *Windows*, suivez les directives suivantes:
1. [Téléchargez le fichier `.zip`.][zip]
1. Extraire le fichier exécutable `.exe`.
1. Créez le dossier `~/bin`. Cela peut se faire avec la commande suivante dans
	git-bash.
	```
	mkdir -p ~/bin
	```
1. Placez le fichier `stm32flash.exe` dans le dossier `~/bin`.
1. Ajoutez le dossier `~/bin` à la variable `PATH` de `bash`. Pour se faire,
	exécutez la commande suivante dans git-bash.
	```
	code ~/.bashrc
	```
	Cela va ouvrir le fichier de configuration pour `bash` dans *Visual Studio
	Code*. Par la suite, ajoutez la ligne suivante, si elle n'existe pas déjà, et
	sauvegardez le fichier.
	```
	export PATH="$PATH:$HOME/bin"
	```
1. [Vérifiez l'installation](#vérifiez-linstallation) (voir ci-dessous)

#### Installation dans *macOS*
Pour *macOS*, suivez les directives suivantes:

1. Ouvrer l'application "Terminal":
	<kbd>Command+space</kbd> ou dans "Launcher"
1. Clonez le répertoire:
	```
	git clone git clone https://git.code.sf.net/p/stm32flash/code stm32flash-code
	```
1. Ouvrez le nouveau dossier:
	```
	cd stm32flash-code
	```
1. Compilez le code:
	```
	make
	```
1. Installez le programme:
	```
	sudo make install
	```
1. [Vérifiez l'installation](#vérifiez-linstallation) (voir ci-dessous)

#### Vérifiez l'installation.
Dans git-bash ou Terminal, exécutez les commandes suivantes.
```
source ~/.bashrc
stm32flash
```
Le résultat devrait être similaire à ce qui suit:
<details>
	<summary>Cliquez ici pour voir les détailles</summary>

	stm32flash 0.5

	http://stm32flash.sourceforge.net/

	ERROR: Device not specified
	Usage: C:\Users\marcel\bin\stm32flash.exe [-bvngfhc] [-[rw] filename] [tty_device | i2c_device]
		-a bus_address	Bus address (e.g. for I2C port)
		-b rate				 Baud rate (default 57600)
		-m mode				 Serial port mode (default 8e1)
		-r filename		 Read flash to file (or - stdout)
		-w filename		 Write flash from file (or - stdout)
		-C							Compute CRC of flash content
		-u							Disable the flash write-protection
		-j							Enable the flash read-protection
		-k							Disable the flash read-protection
		-o							Erase only
		-e n						Only erase n pages before writing the flash
		-v							Verify writes
		-n count				Retry failed writes up to count times (default 10)
		-g address			Start execution at specified address (0 = flash start)
		-S address[:length]		 Specify start address and optionally length for
														read/write/erase operations
		-F RX_length[:TX_length]	Specify the max length of RX and TX frame
		-s start_page	 Flash at specified page (0 = flash start)
		-f							Force binary parser
		-h							Show this help
		-c							Resume the connection (don't send initial INIT)
										*Baud rate must be kept the same as the first init*
										This is useful if the reset fails
		-i GPIO_string	GPIO sequence to enter/exit bootloader mode
										GPIO_string=[entry_seq][:[exit_seq]]
										sequence=[-]n[,sequence]
		-R							Reset device at exit.

	Examples:
		Get device information:
				C:\Users\marcel\bin\stm32flash.exe /dev/ttyS0
			or:
				C:\Users\marcel\bin\stm32flash.exe /dev/i2c-0

		Write with verify and then start execution:
			C:\Users\marcel\bin\stm32flash.exe -w filename -v -g 0x0 /dev/ttyS0

		Read flash to file:
			C:\Users\marcel\bin\stm32flash.exe -r filename /dev/ttyS0

		Read 100 bytes of flash from 0x1000 to stdout:
			C:\Users\marcel\bin\stm32flash.exe -r - -S 0x1000:100 /dev/ttyS0

		Start execution:
			C:\Users\marcel\bin\stm32flash.exe -g 0x0 /dev/ttyS0

		GPIO sequence:
		- entry sequence: GPIO_3=low, GPIO_2=low, GPIO_2=high
		- exit sequence: GPIO_3=high, GPIO_2=low, GPIO_2=high
			C:\Users\marcel\bin\stm32flash.exe -R -i -3,-2,2:3,-2,2 /dev/ttyS0
</details>

[stm32flash]: https://sourceforge.net/p/stm32flash/wiki/Home/
[zip]: https://sourceforge.net/projects/stm32flash/files/stm32flash-0.5-win64.zip/download

## Partie B : Programme "Hello World!"
Un projet *PlatformIO* se trouve dans ce répertoire. Malheureusement, on n'a pas
encore assez de cartes STM32 pour tout le monde. Pour l'instant, vous pourrez
seulement compiler le projet. Il serait possible de flasher votre programme sur
une carte lors du prochain atelier.

Si ce n'est pas déjà fait, cloner le répertoire avec les commandes suivantes
(dans git bash):
1. Créez un dossier pour le répertoire:
	```
	mkdir -p ~/code/grum/eurobot2019/exercices/exercices-pour-stm32
	```
1. Clonez le répertoire:
	```
	git clone ssh://gitlab@gitlab.robitaille.host:49/grum/eurobot2019/exercices/exercices-pour-stm32/exercice-stm32-1-installation-et-hello-world.git ~/code/grum/eurobot2019/exercices/exercices-pour-stm32/exercice-stm32-1-installation-et-hello-world
	```
1. Ouvrez le dossier:
	```
	cd ~/code/grum/eurobot2019/exercices/exercices-pour-stm32/exercice-stm32-1-installation-et-hello-world
	```
1. Ouvrez le dossier dans *Visual Studio Code*:
	```
	code .
	```

Lisez le code dans `./src/main.cpp` et essayez de prédire son fonctionnement.
Par la suite, compiler le code. Si vous avez oublié comment le faire, l'image
ci-dessous l'explique (ouvrez le menu avec <kbd>ctrl-shift-p</kbd>).

![pio build](./images/pio-build.png)

Une fois que vous avez une carte (à l'atelier), télécharger le programme sur la carte comme suit:

![pio upload](./images/pio-upload.png)

