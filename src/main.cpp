#include "mbed.h"

DigitalOut led(LED1);

int main() {
	while(true) {
		led = true;
		wait(1);
		led = false;
		wait(1);
	}
}

